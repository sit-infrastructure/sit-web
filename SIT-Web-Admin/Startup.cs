﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SIT_Web_Admin.Startup))]
namespace SIT_Web_Admin
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
