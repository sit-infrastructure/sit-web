﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Configuration;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace SIT_Web_Admin
{
    public partial class add_data : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["LoginInfo"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                Label user_label = FindControl("Username") as Label;
                user_label.Text = "Welcome " + Request.Cookies["LoginInfo"]["name"];
            }
        }

        protected void dtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadDynamicForm();
        }

        private void loadDynamicForm()
        {
            string selection = (FindControl("dtype") as DropDownList).SelectedValue;
            PlaceHolder form_container = FindControl("form_insertion") as PlaceHolder;
            form_container.Controls.Clear();
            Control selected_form = null;
            if (selection.Equals("inspector"))
            {
                selected_form = LoadControl("~/Controls/InspectorInputForm.ascx");
                selected_form.ID = "Inspector_form";
            }
            else if (selection.Equals("structure"))
            {
                selected_form = LoadControl("~/Controls/StructureInputForm.ascx");
                selected_form.ID = "structure_form";
            }
            else if (selection.Equals("defect_type"))
            {
                selected_form = LoadControl("~/Controls/DefectTypeForm.ascx");
                selected_form.ID = "defect_type_form";
            }
            else if (selection.Equals("structure_elem"))
            {
                selected_form = LoadControl("~/Controls/StructureElementForm.ascx");
                selected_form.ID = "structure_elem_form";
            }
            else if (selection.Equals("loc_desc"))
            {
                selected_form = LoadControl("~/Controls/LocationDescriptionForm.ascx");
                selected_form.ID = "ld_form";
            }
            else if (selection.Equals("associate_inspector"))
            {
                selected_form = LoadControl("~/Controls/AssociateInspectorForm.ascx");
                selected_form.ID = "associate_inspector_form";
            }
            else if (selection.Equals("create_inspection"))
            {
                selected_form = LoadControl("~/Controls/CreateInspection.ascx");
                selected_form.ID = "create_inspection_form";
            }
            if (selected_form != null)
            {
                FindControl("form_insertion").Controls.Add(selected_form);
                (FindControl("form_submission") as Button).Enabled = true;
            }
            else
                (FindControl("form_submission") as Button).Enabled = false;
        }

        protected void form_submission_Click(object sender, EventArgs e)
        {
            string selection = (FindControl("dtype") as DropDownList).SelectedValue;
            string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
            if (selection.Equals("inspector"))
                submit_inspector(endpoint_url);
            else if (selection.Equals("structure"))
                submit_structure(endpoint_url);
            else if (selection.Equals("defect_type"))
                submit_hierarchy_element(endpoint_url);
            else if (selection.Equals("structure_elem"))
                submit_hierarchy_element(endpoint_url);
            else if (selection.Equals("loc_desc"))
                submit_hierarchy_element(endpoint_url);
            else if (selection.Equals("associate_inspector"))
                submit_associate_inspector(endpoint_url);
            else if (selection.Equals("create_inspection"))
                submit_inspection(endpoint_url);

        }

        #region form_submission_methods
        private void submit_inspection(string endpoint_url)
        {
            string request_url = endpoint_url + "/inspection/insert";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
            request.ContentType = "application/json";
            request.Method = "POST";
            PlaceHolder placeholder = FindControl("form_insertion") as PlaceHolder;
            DropDownList structure_list = placeholder.Controls[0].Controls[1] as DropDownList;
            TextBox inspection_name = placeholder.Controls[0].Controls[3] as TextBox;

            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    inspection = inspection_name.Text,
                    structure = "_:" + structure_list.SelectedValue,
                    user = Request.Cookies["LoginInfo"]["session_key"],
                    json = "true"
                });
                streamWrite.Write(json);
            }

            string result;
            var response = (HttpWebResponse)request.GetResponse();
            using (var streamRead = new StreamReader(response.GetResponseStream()))
                result = streamRead.ReadToEnd();
            // TODO: check result for errors here!!!
            Response.Redirect("add_data.aspx");
        }

        private void submit_hierarchy_element(string endpoint_url)
        {
            string request_str = endpoint_url + "/hierarchy/element/insert";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_str);
            request.ContentType = "application/json";
            request.Method = "POST";

            PlaceHolder placeholder = FindControl("form_insertion") as PlaceHolder;
            TreeView parent_tree = placeholder.Controls[0].Controls[5] as TreeView;
            TextBox structure_element_name = placeholder.Controls[0].Controls[3] as TextBox;
            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    parent = "_:" + parent_tree.SelectedNode.Value,
                    elementName = structure_element_name.Text,
                    user = Request.Cookies["LoginInfo"]["session_key"],
                    json = "true"
                });
                streamWrite.Write(json);
            }

            string result;
            var response = (HttpWebResponse)request.GetResponse();
            using (var streamRead = new StreamReader(response.GetResponseStream()))
                result = streamRead.ReadToEnd();
            // TODO: check result for errors here!!!
            Response.Redirect("add_data.aspx");
        }

        private void submit_associate_inspector(string endpoint_url)
        {
            string request_str = endpoint_url + "/inspector/associate";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_str);
            request.ContentType = "application/json";
            request.Method = "POST";

            PlaceHolder placeholder = FindControl("form_insertion") as PlaceHolder;
            DropDownList inspection_list = placeholder.Controls[0].Controls[3] as DropDownList;
            DropDownList inspector_list = placeholder.Controls[0].Controls[5] as DropDownList;
            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    inspection = "_:" + inspection_list.SelectedValue,
                    inspector = "_:" + inspector_list.SelectedValue,
                    user = Request.Cookies["LoginInfo"]["session_key"],
                    json = "true"
                });
                streamWrite.Write(json);
            }
            string result;
            var response = (HttpWebResponse)request.GetResponse();
            using (var streamRead = new StreamReader(response.GetResponseStream()))
                result = streamRead.ReadToEnd();
            // TODO: check result for errors here!!!
            Response.Redirect("add_data.aspx");
        }

        private void submit_structure(string endpoint_url)
        {
            string request_str = endpoint_url + "/structure/insert";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_str);
            request.ContentType = "application/json";
            request.Method = "POST";

            PlaceHolder placeholder = FindControl("form_insertion") as PlaceHolder;
            TextBox structure_name = placeholder.Controls[0].Controls[1] as TextBox;
            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    structure = structure_name.Text,
                    user = Request.Cookies["LoginInfo"]["session_key"],
                    json = "true"
                });
                streamWrite.Write(json);
            }
            string result;
            var response = (HttpWebResponse)request.GetResponse();
            using (var streamRead = new StreamReader(response.GetResponseStream()))
                result = streamRead.ReadToEnd();
            // TODO: check result for errors here!!!
            Response.Redirect("add_data.aspx");
        }

        private void submit_inspector(string endpoint_url)
        {
            string request_str = endpoint_url + "/inspector/insert";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_str);
            request.ContentType = "application/json";
            request.Method = "POST";

            PlaceHolder placeholder = FindControl("form_insertion") as PlaceHolder;
            TextBox inspector_first = placeholder.Controls[0].Controls[1] as TextBox;
            TextBox inspector_last = placeholder.Controls[0].Controls[3] as TextBox;
            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    firstname = inspector_first.Text,
                    lastname = inspector_last.Text,
                    user = Request.Cookies["LoginInfo"]["session_key"],
                    json = "true"
                });
                streamWrite.Write(json);
            }

            string result;
            var response = (HttpWebResponse)request.GetResponse();
            using (var streamRead = new StreamReader(response.GetResponseStream()))
                result = streamRead.ReadToEnd();
            // TODO: check result for errors here!!!
            Response.Redirect("add_data.aspx");
        }
        #endregion

        protected void logout_Click(object sender, EventArgs e)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
            string request_url = endpoint_url + "/login";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
            request.ContentType = "application/json";
            request.Method = "POST";

            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = serializer.Serialize(new
                {
                    session_key = Request.Cookies["LoginInfo"]["session_key"],
                    json = "true"
                });
                streamWrite.Write(json);
            }

            //string result;
            //var response = (HttpWebResponse)request.GetResponse();
            //using (var streamRead = new StreamReader(response.GetResponseStream()))
            //    result = streamRead.ReadToEnd();
            
            Label user_label = FindControl("Username") as Label;
            user_label.Text = "";

            HttpCookie login_cookie = new HttpCookie("LoginInfo");
            login_cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(login_cookie);
            Response.Redirect("Default.aspx");
        }
    }
}