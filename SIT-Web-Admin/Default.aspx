﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SIT_Web_Admin.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <%--<link rel="icon" href="../../favicon.ico">--%>
    <title>Structural Inspection Tool</title>

    <!-- Bootstrap core CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet"/>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="Content/ie10-viewport-bug-workaround.css" rel="stylesheet"/>

    <!-- Custom styles for this template -->
    <link href="Content/jumbotron.css" rel="stylesheet"/>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="Scripts/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="Default.aspx">SIT</a>
            <a class="navbar-brand" href="add_data.aspx">Add Data</a>
            <%--<a class="navbar-brand" href="create_inspection.aspx">Create Inspection</a>--%>
            <a class="navbar-brand" href="search.aspx">Inspection Search</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" runat="server">
            <div class="form-group">
              <asp:TextBox runat="server" CssClass="form-control" Text="Username" ID="user"/>
              <%--<input type="text" placeholder="Email" class="form-control"/>--%>
            </div>
            <div class="form-group">
                <asp:TextBox runat="server" CssClass="form-control" TextMode="Password" ID="pass" />
              <%--<input type="password" placeholder="Password" class="form-control"/>--%>
            </div>
              <asp:Button runat="server" Text="Sign in" CssClass="btn btn-success" ID="login" OnClick="login_Click"/>
            <%--<button type="submit" class="btn btn-success">Sign in</button>--%>
              <asp:Label runat="server" Visible="false" Text="" CssClass="label" ID="Username"/>
              <asp:Button runat="server" Visible="false" Enabled="false" Text="Logout" CssClass="btn" ID="logout" OnClick="logout_Click" />
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <div class="jumbotron">
      <div class="container">
        <h1>SIT - Web Administrator Portal</h1>
        <p>Welcome to the Structural Inspection Tool administrator portal. Use this site to interact with the SIT data store to add ontologies, create and manage new inspections, and lookup data from previous inspections. You must be logged in with an administrator account to fully use this site.</p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-6">
          <h2>Add Data</h2>
          <p>Use this page to manage the ontologies for defects.</p>
          <p><a class="btn btn-default" href="add_data.aspx" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-6">
          <h2>Inspection Search</h2>
          <p>Search for data from previous inspections here.</p>
          <p><a class="btn btn-default" href="search.aspx" role="button">View details &raquo;</a></p>
        </div>
      </div>

      <hr/>

      <footer>
        <p>&copy; 2015 Structural Inspection Tool</p>
      </footer>
    </div> 
</body>
</html>
