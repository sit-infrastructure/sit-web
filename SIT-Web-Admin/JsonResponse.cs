﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIT_Web_Admin
{
    class JsonResponse
    {
        public JsonHead head;
        public JsonResults results;
    }

    public class JsonResults
    {
        public List<Dictionary<string, Dictionary<string, string>>> bindings;
    }

    class JsonHead
    {
        public List<string> vars;
    }
}
