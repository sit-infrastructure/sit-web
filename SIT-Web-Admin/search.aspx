﻿<%@ Register TagPrefix="dbwc" Namespace="DBauer.Web.UI.WebControls" Assembly="DBauer.Web.UI.WebControls.DynamicControlsPlaceholder" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="search.aspx.cs" Inherits="SIT_Web_Admin.search" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <%--<link rel="icon" href="../../favicon.ico">--%>
    <title>Structural Inspection Tool</title>

    <!-- Bootstrap core CSS -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="Content/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="Content/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="Scripts/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="Default.aspx">SIT</a>
            <a class="navbar-brand" href="add_data.aspx">Add Data</a>
            <%--<a class="navbar-brand" href="create_inspection.aspx">Create Inspection</a>--%>
            <a class="navbar-brand" href="search.aspx">Inspection Search</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
              <asp:Label runat="server" Visible="false" Text="" CssClass="label" ID="Username"/>
              <%--<asp:Button runat="server" Visible="false" Enabled="false" Text="Logout" CssClass="btn" ID="logout" OnClick="logout_Click" />--%>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    
    <div class="jumbotron">
      <div class="container">
        <h2>SIT - Search</h2>
        <p>Welcome to the Structural Inspection Tool administrator portal. Use this page to search the SIT data store.</p>
      </div>
    </div>

    <div class="container">
        <form id="form1" class ="form-horizontal" runat="server">
            <div class="form-group">
                <label for="dtype" class="col-sm-2 control-label">Data Type</label>
                <div class="col-sm-10">
                    <asp:DropDownList CssClass="form-control" ID="dtype" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dtype_SelectedIndexChanged">
                        <asp:ListItem Value=""></asp:ListItem>
                        <%--<asp:ListItem Value="inspector">Inspector</asp:ListItem>--%>
                        <asp:ListItem Value="structure">Structure</asp:ListItem>
                        <asp:ListItem Value="inspection">Inspection</asp:ListItem>
                        <asp:ListItem Value="defect">Defect</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <dbwc:DynamicControlsPlaceholder ID="form_insertion" runat="server" ControlsWithoutIDs="Persist"></dbwc:DynamicControlsPlaceholder>
                <asp:Button ID="form_submission" runat="server" CssClass="col-sm-2" Text="Search" OnClick="form_submission_Click" Enabled="false"/>
            </div>
            <dbwc:DynamicControlsPlaceholder ID="search_result_holder" runat="server" ControlsWithoutIDs="Persist">
                <%--<asp:Table runat="server" ID="search_results" Visible="false" CssClass="table-bordered"></asp:Table>--%>
            </dbwc:DynamicControlsPlaceholder>
        </form>
    </div>
</body>
</html>
