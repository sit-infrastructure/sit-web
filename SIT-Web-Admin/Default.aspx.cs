﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIT_Web_Admin
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.Cookies["LoginInfo"] != null)
            {
                FindControl("user").Visible = false;
                FindControl("pass").Visible = false;
                Button login_btn = FindControl("login") as Button;
                login_btn.Visible = false;
                login_btn.Enabled = false;
                Label user_label = FindControl("Username") as Label;
                user_label.Visible = true;
                user_label.Text = "Welcome " + Request.Cookies["LoginInfo"]["name"];
                Button logout_btn = FindControl("logout") as Button;
                logout_btn.Visible = true;
                logout_btn.Enabled = true;
            }
        }

        protected void login_Click(object sender, EventArgs e)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            TextBox user_box = FindControl("user") as TextBox;
            TextBox pass_box = FindControl("pass") as TextBox;
            string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
            string request_url = endpoint_url + "/login";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
            request.ContentType = "application/json";
            request.Method = "POST";
            string username_str = user_box.Text;

            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = serializer.Serialize(new
                {
                    username = username_str,
                    password = pass_box.Text,
                    json = "true"
                });
                streamWrite.Write(json);
            }

            string result;
            var response = (HttpWebResponse)request.GetResponse();
            using (var streamRead = new StreamReader(response.GetResponseStream()))
                result = streamRead.ReadToEnd();

            Dictionary<string, string> json_result = serializer.Deserialize<Dictionary<string, string>>(result);
            HttpCookie login_cookie = new HttpCookie("LoginInfo");
            login_cookie["name"] = json_result["firstname"] + " " + json_result["lastname"];
            login_cookie["session_key"] = json_result["session_key"];
            Response.Cookies.Add(login_cookie);
            pass_box.Visible = false;
            pass_box.Text = "";
            user_box.Visible = false;
            user_box.Text = "";
            Button login_btn = FindControl("login") as Button;
            login_btn.Visible = false;
            login_btn.Enabled = false;
            Label user_label = FindControl("Username") as Label;
            user_label.Visible = true;
            user_label.Text = "Welcome " + login_cookie["name"];
            Button logout_btn = FindControl("logout") as Button;
            logout_btn.Visible = true;
            logout_btn.Enabled = true;
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
            string request_url = endpoint_url + "/login";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
            request.ContentType = "application/json";
            request.Method = "POST";

            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = serializer.Serialize(new
                {
                    session_key = Request.Cookies["LoginInfo"]["session_key"],
                    json = "true"
                });
                streamWrite.Write(json);
            }

            //string result;
            //var response = (HttpWebResponse)request.GetResponse();
            //using (var streamRead = new StreamReader(response.GetResponseStream()))
            //    result = streamRead.ReadToEnd();

            FindControl("user").Visible = true;
            FindControl("pass").Visible = true;
            Button login_btn = FindControl("login") as Button;
            login_btn.Visible = true;
            login_btn.Enabled = true;
            Label user_label = FindControl("Username") as Label;
            user_label.Visible = false;
            user_label.Text = "";
            Button logout_btn = FindControl("logout") as Button;
            logout_btn.Visible = false;
            logout_btn.Enabled = false;

            HttpCookie login_cookie = new HttpCookie("LoginInfo");
            login_cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(login_cookie);
        }
    }
}