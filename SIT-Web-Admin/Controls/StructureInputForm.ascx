﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StructureInputForm.ascx.cs" Inherits="SIT_Web_Admin.StructureInputForm" %>

<div class="form-group">
    <label for="structure_name" class="col-sm-2 control-label">Structure Name</label>
    <div class="col-sm-10">
        <asp:TextBox runat="server" ID="structure_name" CssClass="form-control"></asp:TextBox>
    </div>
</div>
<%--<div class="form-group">
    <label for="structure_loc" class="col-sm-2 control-label">Location</label>
    <div class="col-sm-10">
        <asp:TextBox runat="server" ID="structure_loc" CssClass="form-control"></asp:TextBox>
    </div>
</div>--%>