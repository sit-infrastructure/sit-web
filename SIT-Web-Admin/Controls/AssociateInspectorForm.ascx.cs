﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIT_Web_Admin.Controls
{
    public partial class AssociateInspectorForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DropDownList structure_list = FindControl("structure") as DropDownList;
            DropDownList inspection_list = FindControl("inspection") as DropDownList;
            DropDownList inspector_list = FindControl("inspector") as DropDownList;
            if (!(structure_list.Items.Count > 1))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
                string request_url = endpoint_url + "/structure/all";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }

                string result;
                var response = (HttpWebResponse)request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();
                // TODO: Check for errors here!!!
                JsonResponse structures = serializer.Deserialize<JsonResponse>(result);
                foreach (Dictionary<string, Dictionary<string, string>> structure in structures.results.bindings)
                {
                    string text = structure["name"]["value"];
                    string val = structure["structure"]["value"];
                    structure_list.Items.Add(new ListItem(text, val));
                }

                request_url = endpoint_url + "/inspection/all";
                request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }
                
                response = (HttpWebResponse)request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();
                JsonResponse inspections = serializer.Deserialize<JsonResponse>(result);
                foreach(Dictionary<string, Dictionary<string, string>> inspection in inspections.results.bindings)
                {
                    string text = inspection["name"]["value"];
                    string val = inspection["inspection"]["value"];
                    inspection_list.Items.Add(new ListItem(text, val));
                }

                request_url = endpoint_url + "/inspector/all";
                request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }

                response = (HttpWebResponse)request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();
                JsonResponse inspectors = serializer.Deserialize<JsonResponse>(result);
                foreach (Dictionary<string, Dictionary<string, string>> inspector in inspectors.results.bindings)
                {
                    string text = inspector["firstname"]["value"] + " " + inspector["lastname"]["value"];
                    string val = inspector["inspector"]["value"];
                    inspector_list.Items.Add(new ListItem(text, val));
                }
            }
        }

        protected void structure_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownList structure_list = FindControl("structure") as DropDownList;
            DropDownList inspection_list = FindControl("inspection") as DropDownList;
            inspection_list.Items.Clear();
            inspection_list.Items.Add(new ListItem("", ""));
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
            string result;

            if (structure_list.SelectedValue.Equals(""))
            {
                string request_url = endpoint_url + "/inspection/all";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }
                var response = (HttpWebResponse)request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();
            }
            else
            {
                string request_url = endpoint_url + "/structure/inspections";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        structure = "_:" + structure_list.SelectedValue,
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }
                var response = (HttpWebResponse)request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();
            }

            JsonResponse inspections = serializer.Deserialize<JsonResponse>(result);
            foreach (Dictionary<string, Dictionary<string, string>> inspection in inspections.results.bindings)
            {
                string text = inspection["name"]["value"];
                string val = inspection["inspection"]["value"];
                inspection_list.Items.Add(new ListItem(text, val));
            }
        }
    }
}