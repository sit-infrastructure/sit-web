﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StructureElementForm.ascx.cs" Inherits="SIT_Web_Admin.Controls.StructureElementForm" %>

<div class="form-group">
    <label for="structure" class="col-sm-2 control-label">Structure</label>
    <div class="col-sm-10">
        <asp:DropDownList runat="server" ID="structure" CssClass="col-sm-10 form-control" AutoPostBack="true" OnSelectedIndexChanged="structure_SelectedIndexChanged">
            <asp:ListItem Value=""></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>

<div class="form-group">
    <label for="structure_elem_name" class="col-sm-2 control-label">Structure Element Name</label>
    <div class="col-sm-10">
        <asp:TextBox runat="server" ID="structure_elem_name" CssClass="form-control"></asp:TextBox>
    </div>
</div>

<div class="form-group">
    <label for="structure_elem_parent" class="col-sm-2 control-label">Structure Element Parent</label>
    <div class="col-sm-10">
        <asp:TreeView runat="server" ID="structure_elem_parent" CssClass="col-sm-10" ExpandDepth="2" SelectedNodeStyle-Font-Bold="true">
            <Nodes>
            </Nodes>
        </asp:TreeView>
    </div>
</div>