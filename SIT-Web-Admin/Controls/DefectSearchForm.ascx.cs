﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIT_Web_Admin.Controls
{
    public partial class DefectSearchForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DropDownList inspection_list = FindControl("inspection") as DropDownList;
            DropDownList inspector_list = FindControl("inspector") as DropDownList;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (!(inspection_list.Items.Count > 1))
            {
                string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
                string request_url = endpoint_url + "/inspection/all";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";
                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }

                string result;
                var response = request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();

                JsonResponse inspections = serializer.Deserialize<JsonResponse>(result);
                foreach (Dictionary<string, Dictionary<string, string>> inspection in inspections.results.bindings)
                {
                    string inspection_bnode = inspection["inspection"]["value"];
                    string inspection_name = inspection["name"]["value"];
                    inspection_list.Items.Add(new ListItem(inspection_name, inspection_bnode));
                }

                request_url = endpoint_url + "/inspector/all";
                request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }

                response = (HttpWebResponse)request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();
                JsonResponse inspectors = serializer.Deserialize<JsonResponse>(result);
                foreach (Dictionary<string, Dictionary<string, string>> inspector in inspectors.results.bindings)
                {
                    string text = inspector["firstname"]["value"] + " " + inspector["lastname"]["value"];
                    string val = inspector["inspector"]["value"];
                    inspector_list.Items.Add(new ListItem(text, val));
                }
            }
        }
    }
}