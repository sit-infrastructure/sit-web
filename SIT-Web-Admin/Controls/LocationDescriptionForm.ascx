﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationDescriptionForm.ascx.cs" Inherits="SIT_Web_Admin.Controls.LocationDescriptionForm" %>

<div class="form-group">
    <label for="structure" class="col-sm-2 control-label">Structure</label>
    <div class="col-sm-10">
        <asp:DropDownList runat="server" ID="structure" CssClass="col-sm-10 form-control" 
            AutoPostBack="true" OnSelectedIndexChanged="structure_SelectedIndexChanged">
            <asp:ListItem Value=""></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>

<div class="form-group">
    <label for="ld_name" class="col-sm-2 control-label">Defect Type Name</label>
    <div class="col-sm-10">
        <asp:TextBox runat="server" ID="ld_name" CssClass="col-sm-10 form-control"></asp:TextBox>
    </div>
</div>

<div class="form-group">
    <label for="ld_parent" class="col-sm-2 control-label">Defect Type Parent</label>
    <div class="col-sm-10">
        <asp:TreeView runat="server" ID="ld_parent" CssClass="col-sm-10" ExpandDepth="2" SelectedNodeStyle-Font-Bold="true">
            <Nodes>
            </Nodes>
        </asp:TreeView>
    </div>
</div>