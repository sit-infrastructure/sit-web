﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssociateInspectorForm.ascx.cs" Inherits="SIT_Web_Admin.Controls.AssociateInspectorForm" %>

<div class="form-group">
    <label for="structure" class="col-sm-2 control-label">Structure (optional)</label>
    <div class="col-sm-10">
        <asp:DropDownList runat="server" ID="structure" CssClass="col-sm-10 form-control" AutoPostBack="true" OnSelectedIndexChanged="structure_SelectedIndexChanged">
            <asp:ListItem Value=""></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
<div class="form-group">
    <label for="structure" class="col-sm-2 control-label">Inspection</label>
    <div class="col-sm-10">
        <asp:DropDownList runat="server" ID="inspection" CssClass="col-sm-10 form-control" AutoPostBack="true">
            <asp:ListItem Value=""></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
<div class="form-group">
    <label for="structure" class="col-sm-2 control-label">Inspector</label>
    <div class="col-sm-10">
        <asp:DropDownList runat="server" ID="inspector" CssClass="col-sm-10 form-control" AutoPostBack="true">
            <asp:ListItem Value=""></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>