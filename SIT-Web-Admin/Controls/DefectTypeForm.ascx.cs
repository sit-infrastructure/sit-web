﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIT_Web_Admin.Controls
{
    public partial class DefectTypeForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DropDownList structure_list = FindControl("structure") as DropDownList;
            if (!(structure_list.Items.Count > 1))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
                string request_url = endpoint_url + "/structure/all";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }

                string result;
                var response = (HttpWebResponse)request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();
                // TODO: Check for errors here!!!
                JsonResponse structures = serializer.Deserialize<JsonResponse>(result);
                foreach (Dictionary<string, Dictionary<string, string>> structure in structures.results.bindings)
                {
                    string text = structure["name"]["value"];
                    string val = structure["structure"]["value"];
                    structure_list.Items.Add(new ListItem(text, val));
                }
            }
        }

        protected void structure_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected_structure = (FindControl("structure") as DropDownList).SelectedValue;
            TreeView tree = FindControl("defect_type_parent") as TreeView;
            tree.Nodes.Clear();
            if(!selected_structure.Equals(""))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
                string request_url = endpoint_url + "/structure/all";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }

                string result;
                var response = (HttpWebResponse)request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();
                JsonResponse structures = serializer.Deserialize<JsonResponse>(result);
                string hierarchy_val = "";
                foreach(Dictionary<string, Dictionary<string, string>> structure in structures.results.bindings)
                {
                    if (structure["structure"]["value"].Equals(selected_structure))
                    {
                        hierarchy_val = structure["dtHier"]["value"];
                        break;
                    }
                }
                TreeNode root_node = new TreeNode("Root", hierarchy_val);
                tree.Nodes.Add(root_node);

                request_url = endpoint_url + "/hierarchy/elements";
                request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        hierarchy = "_:" + hierarchy_val,
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }
                response = (HttpWebResponse)request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();
                JsonResponse hierarchy = serializer.Deserialize<JsonResponse>(result);
                List<Dictionary<string, Dictionary<string, string>>> not_added;
                Dictionary<string, string> value_paths = new Dictionary<string, string>();
                value_paths[hierarchy_val] = hierarchy_val;
                foreach (Dictionary<string, Dictionary<string, string>> element in hierarchy.results.bindings)
                {
                    string parent_val_path = value_paths[element["parent"]["value"]];
                    TreeNode parent_node = tree.FindNode(parent_val_path);
                    if (parent_node != null)
                    {
                        string val = element["element"]["value"];
                        TreeNode new_node = new TreeNode(element["elementName"]["value"], val);
                        parent_node.ChildNodes.Add(new_node);
                        value_paths[val] = new_node.ValuePath;
                    }
                }
            }
        }
    }
}