﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIT_Web_Admin.Controls
{
    public partial class CreateInspection : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DropDownList structure_list = FindControl("inspection_structure") as DropDownList;
            if(!(structure_list.Items.Count > 1))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
                string request_url = endpoint_url + "/structure/all";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);
                }

                string result;
                var response = (HttpWebResponse)request.GetResponse();
                using (var streamRead = new StreamReader(response.GetResponseStream()))
                    result = streamRead.ReadToEnd();
                // TODO: Check for errors here!!!
                JsonResponse structures = serializer.Deserialize<JsonResponse>(result);
                foreach (Dictionary<string, Dictionary<string, string>> structure in structures.results.bindings)
                {
                    string text = structure["name"]["value"];
                    string val = structure["structure"]["value"];
                    structure_list.Items.Add(new ListItem(text, val));
                }
            }
        }
    }
}