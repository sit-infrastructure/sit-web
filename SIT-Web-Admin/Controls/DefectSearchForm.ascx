﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefectSearchForm.ascx.cs" Inherits="SIT_Web_Admin.Controls.DefectSearchForm" %>

<div class="form-group">
    <label for="inspection" class="col-sm-2 control-label">Inspection</label>
    <div class="col-sm-4">
        <asp:DropDownList runat="server" ID="inspection" CssClass="col-sm-4 form-control">
            <asp:ListItem Value=""></asp:ListItem>
        </asp:DropDownList>
    </div>
    <label for="inspector" class="col-sm-2 control-label">Inspector</label>
    <div class="col-sm-4">
        <asp:DropDownList runat="server" ID="inspector" CssClass="col-sm-4 form-control">
            <asp:ListItem Value=""></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
<div class="form-group">
    <label for="description" class="col-sm-2 control-label">Description</label>
    <asp:TextBox runat="server" ID="description" CssClass="col-sm-10 form-control"></asp:TextBox>
</div>