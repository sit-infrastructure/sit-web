﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateInspection.ascx.cs" Inherits="SIT_Web_Admin.Controls.CreateInspection" %>



<div class="form-group">
    <label for="structure" class="col-sm-2 control-label">Structure</label>
    <div class="col-sm-10">
        <asp:DropDownList CssClass="form-control" ID="inspection_structure" runat="server">
            <asp:ListItem Value=""></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
<div class="form-group">
    <label for="inspection_name" class="col-sm-2 control-label">Inspection Name</label>
    <div class="col-sm-10">
        <asp:TextBox runat="server" ID="inspection_name" CssClass="form-control"></asp:TextBox>
    </div>
</div>