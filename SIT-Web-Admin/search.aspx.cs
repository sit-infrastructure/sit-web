﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIT_Web_Admin
{
    public partial class search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["LoginInfo"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                Label user_label = FindControl("Username") as Label;
                user_label.Text = "Welcome " + Request.Cookies["LoginInfo"]["name"];
            }
        }

        protected void dtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selection = (FindControl("dtype") as DropDownList).SelectedValue;
            PlaceHolder placeholder = FindControl("form_insertion") as PlaceHolder;
            placeholder.Controls.Clear();
            Control additional_form = null;
            if (selection.Equals("defect"))
            {
                additional_form = LoadControl("~/Controls/DefectSearchForm.ascx");
                additional_form.ID = "defect_search_form";
                (FindControl("form_submission") as Button).Enabled = true;
            }
            else if (selection.Equals(""))
                (FindControl("form_submission") as Button).Enabled = false;
            else
                (FindControl("form_submission") as Button).Enabled = true;
            if (additional_form != null)
                placeholder.Controls.Add(additional_form);
        }

        protected void form_submission_Click(object sender, EventArgs e)
        {
            string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
            string selection = (FindControl("dtype") as DropDownList).SelectedValue;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            PlaceHolder result_holder = (FindControl("search_result_holder") as PlaceHolder);
            result_holder.Controls.Clear();
            Table search_results = new Table();
            search_results.Visible = true;
            search_results.ID = "result_table";
            search_results.CssClass = "table-bordered";
            if (selection.Equals("structure"))
                structure_search(endpoint_url, serializer, search_results);
            else if (selection.Equals("inspection"))
                inspection_search(endpoint_url, serializer, search_results);
            else if (selection.Equals("defect"))
                defect_search(endpoint_url, serializer, search_results);
            result_holder.Controls.Add(search_results);
            //else if (selection.Equals("inspector"))
            //    inspector_search(endpoint_url, serializer, search_results);
        }

        protected void delete_button_Click(object sender, EventArgs e)
        {
            string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
            string selection = (FindControl("dtype") as DropDownList).SelectedValue;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string bnode = (sender as Button).ID.Replace("_delete_btn", "");
            if (selection.Equals("structure"))
            {
                string request_url = endpoint_url + "/structure/delete";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";
                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        structure = "_:" + bnode,
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);

                    string result;
                    var response = request.GetResponse();
                    using (var streamRead = new StreamReader(response.GetResponseStream()))
                        result = streamRead.ReadToEnd();
                }
            }
            else if (selection.Equals("inspection"))
            {
                string request_url = endpoint_url + "/inspection/delete";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";
                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        inspection = "_:" + bnode,
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);

                    string result;
                    var response = request.GetResponse();
                    using (var streamRead = new StreamReader(response.GetResponseStream()))
                        result = streamRead.ReadToEnd();
                }
            }
            else if (selection.Equals("defect"))
            {
                string request_url = endpoint_url + "/defect/delete";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
                request.ContentType = "application/json";
                request.Method = "POST";
                using (var streamWrite = new StreamWriter(request.GetRequestStream()))
                {
                    string json = serializer.Serialize(new
                    {
                        defect = "_:" + bnode,
                        user = Request.Cookies["LoginInfo"]["session_key"],
                        json = "true"
                    });
                    streamWrite.Write(json);

                    string result;
                    var response = request.GetResponse();
                    using (var streamRead = new StreamReader(response.GetResponseStream()))
                        result = streamRead.ReadToEnd();
                }
            }
            Response.Redirect("search.aspx");
        }

        private void defect_search(string endpoint_url, JavaScriptSerializer serializer, Table search_results)
        {
            string request_url = endpoint_url + "/defect/search";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
            request.ContentType = "application/json";
            request.Method = "POST";
            PlaceHolder placeholder = FindControl("form_insertion") as PlaceHolder;
            string inspection_val = (placeholder.Controls[0].Controls[1] as DropDownList).SelectedValue;
            string inspector_val = (placeholder.Controls[0].Controls[3] as DropDownList).SelectedValue;
            string description_str = (placeholder.Controls[0].Controls[5] as TextBox).Text;
            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                Dictionary<string, string> search_vals = new Dictionary<string, string>();
                search_vals["description"] = description_str;
                if (!inspection_val.Equals(""))
                    search_vals["inspection"] = "_:" + inspection_val;
                if (!inspector_val.Equals(""))
                    search_vals["inspector"] = "_:" + inspector_val;
                search_vals["user"] = Request.Cookies["LoginInfo"]["session_key"];
                search_vals["json"] = "true";
                string json = serializer.Serialize(search_vals);
                streamWrite.Write(json);
            }

            string result;
            var response = request.GetResponse();
            using (var streamRead = new StreamReader(response.GetResponseStream()))
                result = streamRead.ReadToEnd();
            //TODO: display in table
            TableHeaderRow header = new TableHeaderRow();
            TableCell descr_header = new TableCell();
            descr_header.Text = "Description";
            header.Cells.Add(descr_header);
            TableCell date_header = new TableCell();
            date_header.Text = "Date";
            header.Cells.Add(date_header);
            TableCell inspec_header = new TableCell();
            inspec_header.Text = "Inspection Name";
            header.Cells.Add(inspec_header);
            TableCell last_name_header = new TableCell();
            last_name_header.Text = "Inspector Last Name";
            header.Cells.Add(last_name_header);
            TableCell first_name_header = new TableCell();
            first_name_header.Text = "Inspector First Name";
            header.Cells.Add(first_name_header);
            TableCell latitude_header = new TableCell();
            latitude_header.Text = "Latitude";
            header.Cells.Add(latitude_header);
            TableCell longitude_header = new TableCell();
            longitude_header.Text = "Longitude";
            header.Cells.Add(longitude_header);
            TableCell se_header = new TableCell();
            se_header.Text = "Structural Element";
            header.Cells.Add(se_header);
            TableCell dt_header = new TableCell();
            dt_header.Text = "Defect Type";
            header.Cells.Add(dt_header);
            TableCell ld_header = new TableCell();
            ld_header.Text = "Location Description";
            header.Cells.Add(ld_header);
            //TableCell delete_header = new TableCell();
            //delete_header.Text = "Delete";
            //header.Cells.Add(delete_header);
            search_results.Rows.Add(header);

            JsonResponse defects = serializer.Deserialize<JsonResponse>(result);
            foreach (Dictionary<string, Dictionary<string, string>> defect in defects.results.bindings)
            {
                string defect_bnode = defect["defect"]["value"];
                TableRow defect_row = new TableRow();
                defect_row.ID = defect_bnode + "_row";

                TableCell descr_cell = new TableCell();
                descr_cell.ID = defect_bnode + "_descr_cell";
                descr_cell.Text = defect["description"]["value"];
                defect_row.Cells.Add(descr_cell);

                TableCell date_cell = new TableCell();
                date_cell.ID = defect_bnode + "_date_cell";
                date_cell.Text = defect["datetime"]["value"];
                defect_row.Cells.Add(date_cell);

                TableCell inspection_cell = new TableCell();
                inspection_cell.ID = defect_bnode + "_inspection_cell";
                inspection_cell.Text = defect["inspectionName"]["value"];
                defect_row.Cells.Add(inspection_cell);

                TableCell last_name_cell = new TableCell();
                last_name_cell.ID = defect_bnode + "_last_name_cell";
                last_name_cell.Text = defect["inspectorLastName"]["value"];
                defect_row.Cells.Add(last_name_cell);

                TableCell first_name_cell = new TableCell();
                first_name_cell.ID = defect_bnode + "_first_name_cell";
                first_name_cell.Text = defect["inspectorFirstName"]["value"];
                defect_row.Cells.Add(first_name_cell);

                TableCell lat_cell = new TableCell();
                lat_cell.ID = defect_bnode + "_lat_cell";
                lat_cell.Text = defect["latitude"]["value"];
                defect_row.Cells.Add(lat_cell);

                TableCell long_cell = new TableCell();
                long_cell.ID = defect_bnode + "_long_cell";
                long_cell.Text = defect["longitude"]["value"];
                defect_row.Cells.Add(long_cell);

                TableCell se_cell = new TableCell();
                se_cell.ID = defect_bnode + "_se_cell";
                if(defect.ContainsKey("sename"))
                    se_cell.Text = defect["sename"]["value"];
                defect_row.Cells.Add(se_cell);

                TableCell dt_cell = new TableCell();
                dt_cell.ID = defect_bnode + "_dt_cell";
                if(defect.ContainsKey("dtname"))
                    dt_cell.Text = defect["dtname"]["value"];
                defect_row.Cells.Add(dt_cell);

                TableCell ld_cell = new TableCell();
                ld_cell.ID = defect_bnode + "_ld_cell";
                if(defect.ContainsKey("ldname"))
                    ld_cell.Text = defect["ldname"]["value"];
                defect_row.Cells.Add(ld_cell);

                //TableCell delete_cell = new TableCell();
                //delete_cell.ID = defect_bnode + "_delete_cell";
                //Button delete_button = new Button();
                //delete_button.Enabled = false;
                //delete_button.ID = defect_bnode + "_delete_btn";
                //delete_button.OnClientClick = "return confirm('Are you sure you want to delete?')";
                //delete_button.Text = "Delete";
                //delete_button.Click += new EventHandler(delete_button_Click);
                //delete_cell.Controls.Add(delete_button);
                //defect_row.Cells.Add(delete_cell);

                search_results.Rows.Add(defect_row);
            }
        }

        private void inspection_search(string endpoint_url, JavaScriptSerializer serializer, Table search_results)
        {
            string request_url = endpoint_url + "/inspection/all";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
            request.ContentType = "application/json";
            request.Method = "POST";
            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = serializer.Serialize(new
                {
                    user = Request.Cookies["LoginInfo"]["session_key"],
                    json = "true"
                });
                streamWrite.Write(json);
            }

            string result;
            var response = request.GetResponse();
            using (var streamRead = new StreamReader(response.GetResponseStream()))
                result = streamRead.ReadToEnd();
            TableHeaderRow header = new TableHeaderRow();
            TableCell name_header = new TableCell();
            name_header.Text = "Inspection Name";
            header.Cells.Add(name_header);
            //TableCell delete_header = new TableCell();
            //delete_header.Text = "Delete";
            //header.Cells.Add(delete_header);
            search_results.Rows.Add(header);

            JsonResponse inspections = serializer.Deserialize<JsonResponse>(result);
            foreach (Dictionary<string, Dictionary<string, string>> inspection in inspections.results.bindings)
            {
                string inspection_bnode = inspection["inspection"]["value"];
                TableRow inspection_row = new TableRow();
                inspection_row.ID = inspection_bnode + "_row";

                TableCell name_cell = new TableCell();
                name_cell.ID = inspection_bnode + "_name_cell";
                name_cell.Text = inspection["name"]["value"];
                inspection_row.Cells.Add(name_cell);

                //TableCell delete_cell = new TableCell();
                //delete_cell.ID = inspection_bnode + "_delete_cell";
                //Button delete_button = new Button();
                //delete_button.Enabled = false;
                //delete_button.ID = inspection_bnode + "_delete_btn";
                //delete_button.OnClientClick = "return confirm('Are you sure you want to delete?')";
                //delete_button.Text = "Delete";
                //delete_button.Click += new EventHandler(delete_button_Click);
                //delete_cell.Controls.Add(delete_button);
                //inspection_row.Cells.Add(delete_cell);

                search_results.Rows.Add(inspection_row);
            }
        }

        private void structure_search(string endpoint_url, JavaScriptSerializer serializer, Table search_results)
        {
            string request_url = endpoint_url + "/structure/all";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
            request.ContentType = "application/json";
            request.Method = "POST";
            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = serializer.Serialize(new
                {
                    user = Request.Cookies["LoginInfo"]["session_key"],
                    json = "true"
                });
                streamWrite.Write(json);
            }

            string result;
            var response = request.GetResponse();
            using (var streamRead = new StreamReader(response.GetResponseStream()))
                result = streamRead.ReadToEnd();
            TableHeaderRow header = new TableHeaderRow();
            TableCell name_header = new TableCell();
            name_header.Text = "Structure Name";
            header.Cells.Add(name_header);
            //TableCell delete_header = new TableCell();
            //delete_header.Text = "Delete";
            //header.Cells.Add(delete_header);
            search_results.Rows.Add(header);

            JsonResponse structures = serializer.Deserialize<JsonResponse>(result);
            foreach(Dictionary<string,Dictionary<string,string>> structure in structures.results.bindings)
            {
                string structure_bnode = structure["structure"]["value"];
                TableRow structure_row = new TableRow();
                structure_row.ID = structure_bnode + "_row";

                TableCell name_cell = new TableCell();
                name_cell.ID = structure_bnode + "_name_cell";
                name_cell.Text = structure["name"]["value"];
                structure_row.Cells.Add(name_cell);

                //TableCell delete_cell = new TableCell();
                //delete_cell.ID = structure_bnode + "_delete_cell";
                //Button delete_button = new Button();
                //delete_button.Enabled = false;
                //delete_button.ID = structure_bnode + "_delete_btn";
                //delete_button.OnClientClick = "return confirm('Are you sure you want to delete?')";
                //delete_button.Text = "Delete";
                //delete_button.Click += new EventHandler(delete_button_Click);
                //delete_cell.Controls.Add(delete_button);
                //structure_row.Cells.Add(delete_cell);

                search_results.Rows.Add(structure_row);
            }
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string endpoint_url = WebConfigurationManager.AppSettings["endpoint_url"];
            string request_url = endpoint_url + "/login";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
            request.ContentType = "application/json";
            request.Method = "POST";

            using (var streamWrite = new StreamWriter(request.GetRequestStream()))
            {
                string json = serializer.Serialize(new
                {
                    session_key = Request.Cookies["LoginInfo"]["session_key"],
                    json = "true"
                });
                streamWrite.Write(json);
            }

            //string result;
            //var response = (HttpWebResponse)request.GetResponse();
            //using (var streamRead = new StreamReader(response.GetResponseStream()))
            //    result = streamRead.ReadToEnd();
            Label user_label = FindControl("Username") as Label;
            user_label.Text = "";

            HttpCookie login_cookie = new HttpCookie("LoginInfo");
            login_cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(login_cookie);
            Response.Redirect("Default.aspx");
        }

        //private static void inspector_search(string endpoint_url, JavaScriptSerializer serializer, Table search_results)
        //{
        //    string request_url = endpoint_url + "/inspector/all";
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(request_url);
        //    request.ContentType = "application/json";
        //    request.Method = "POST";
        //    using (var streamWrite = new StreamWriter(request.GetRequestStream()))
        //    {
        //        string json = serializer.Serialize(new
        //        {
        //            json = "true"
        //        });
        //        streamWrite.Write(json);
        //    }

        //    string result;
        //    var response = request.GetResponse();
        //    using (var streamRead = new StreamReader(response.GetResponseStream()))
        //        result = streamRead.ReadToEnd();
        //    //TODO: display in table
        //    TableHeaderRow header = new TableHeaderRow();
        //    TableCell last_header = new TableCell();
        //    last_header.Text = "Last Name";
        //    header.Cells.Add(last_header);
        //    TableCell first_header = new TableCell();
        //    first_header.Text = "First Name";
        //    header.Cells.Add(first_header);
        //    TableCell delete_header = new TableCell();
        //    delete_header.Text = "Delete";
        //    header.Cells.Add(delete_header);

        //    JsonResponse inspectors = serializer.Deserialize<JsonResponse>(result);
        //    foreach(Dictionary<string, Dictionary<string, string>> inspector in inspectors.results.bindings)
        //    {

        //    }
        //}
    }
}